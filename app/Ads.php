<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $table = 'tb_ads';
    protected $fillable = [
        'ads_name','published_by', 'description','ads_image','published_from','published_to','is_active', 'is_feature'
    ];
    public function user(){
    	return $this->belongsTo('App\User','published_by');
    } 
}
