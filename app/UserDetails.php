<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Notifications\AdminResetPasswordNotification;

class UserDetails extends Authenticatable
{
    use Notifiable;

    protected $table = 'tb_user_details';
    protected $fillable = [
        'user_id','full_name', 'street_address','mobile_no'
    ];
    public function user(){
    	return $this->belongsTo('App\User','user_id');
    } 
    public function location(){
    	return $this->belongsTo('App\Location','location_id');
    } 
}
