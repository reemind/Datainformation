<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoLink extends Model
{
    protected $table = 'tb_videolink';
    protected $casts = [
        'description' => 'array'
    ];
}
