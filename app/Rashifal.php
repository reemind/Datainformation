<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rashifal extends Model
{
    protected $table = 'tb_rashifal';
    protected $fillable = [
        'published_by', 'description','is_active'
    ];
    public function user(){
    	return $this->belongsTo('App\User','published_by');
    } 
}
