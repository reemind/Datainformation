<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationInfo extends Model
{
    protected $table = 'tb_organizationinfo';
    
    protected $casts = [
        'openning_day' => 'array'
    ];
    public function user(){
    	return $this->belongsTo('App\User','added_by');
    } 
    public function address(){
    	return $this->belongsTo('App\Address','address_id');
    } 
    public function category(){
    	return $this->belongsTo('App\Category','category_id');
    }
}
