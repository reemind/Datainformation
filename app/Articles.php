<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $table = 'tb_articles';
    protected $fillable = [
        'published_by','title', 'description','file_url','published_data','is_active', 'is_feature'
    ];
    public function user(){
    	return $this->belongsTo('App\User','published_by');
    } 
}
