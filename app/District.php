<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'tb_district';
    public function state()
    {
        return $this->belongsTo('App\State','state_id');
    }
}
