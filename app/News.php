<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'tb_news';
    protected $fillable = [
        'published_by','title', 'description','file_url','published_data','is_active', 'is_feature'
    ];
    public function user(){
    	return $this->belongsTo('App\User','published_by');
    } 
}
