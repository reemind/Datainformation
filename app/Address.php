<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'tb_organization_address';
    
    public function municipality()
    {
        return $this->belongsTo('App\Location','municipality_id');
    }
}
