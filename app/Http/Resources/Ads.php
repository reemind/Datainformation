<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Ads extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // return parent::toArray($request);
       return [
        'id' => $this->id,
        'ads_name' => $this->ads_name,
        'description'=> $this->description,
        'published_by'=> $this->user->email,
        'ads_image'=>$this->ads_image,
        'published_from'=>$this->published_from,
        'published_to'=>$this->published_to,
        'is_active'=>$this->is_active,
        'is_feature'=>$this->is_feature,
    ];
    }
}
