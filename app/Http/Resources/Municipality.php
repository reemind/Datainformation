<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Municipality extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
       return [
        'id' => $this->id,
        'vdv_municipality_name' => $this->vdv_municipality_name,
        'is_active'=>$this->is_active,
       
    ];
    }
}
