<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Rashifal extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // return parent::toArray($request);
       return [
        "id"=>$this->id,
        "published_by"=> $this->user->email,
        "type"=>$this->type,
        "published_from"=>$this->published_from,
        "published_to"=>$this->published_to,
        "description"=>$this->description,
        "aries"=>$this->aries,
        "taurus"=>$this->taurus,
        "gemini"=>$this->gemini,
        "cancer"=>$this->cancer,
        "leo" =>$this->leo,
        "virgo"=>$this->virgo,
        "libra"=>$this->libra,
        "scorpio"=>$this->scorpio,
        "sagittarius"=>$this->sagittarius,
        "capricorn"=>$this->capricorn,
        "aquarius"=>$this->aquarius,
        "pisces"=>$this->pisces,
        "is_active"=>$this->is_active
    ];
    }
}
