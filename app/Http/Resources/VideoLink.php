<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VideoLink extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return[
            'id' => $this->id,
            'videolink' => $this->videolink,
            'video_title' => $this->video_title,
            'description' =>$this->description,
            'is_active'=> $this->is_active,
        ];
    }
}
