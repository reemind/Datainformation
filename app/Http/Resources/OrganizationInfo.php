<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrganizationInfo extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'organization_id'=>$this->id,
            'category_id'=>$this->category->id,
            'category_name'=>$this->category->category_name,
            'category_image'=>$this->category->category_image,
            'organization_name'=>$this->organization_name,
            'phone_no'=>$this->phone_no,
            'organization_email'=>$this->organization_email,
            'openning_day'=>$this->openning_day,
            'openning_time'=>$this->openning_time,
            'services'=>$this->services,
            'feature'=>$this->feature,
            'about_organization'=>$this->about_organization,
            'website'=>$this->website,
            'related_links'=>$this->related_links,
            'banner_image'=>$this->banner_image,
            'profile_image'=>$this->profile_image,
            'added_by'=>$this->user->email,
            'street_address'=> $this->address->street_address,
            'street_address_id'=> $this->address->id,
            'lattitude'=>$this->address->lattitude,
            'longitude'=>$this->address->longitude,
            'municipality_id'=>$this->address->municipality->id,
            'district_name'=>$this->address->municipality->district->district_name,
            'district_id'=>$this->address->municipality->district->id,
            'state_name'=>$this->address->municipality->district->state->state_name,
            'addstate_id'=>$this->address->municipality->district->state->id,
            'is_verified'=>$this->is_verified,
            'is_active'=>$this->is_active,
            'is_feature'=>$this->is_feature
        ];
    }
}
