<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Profiledata extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //  return parent::toArray($request);
        return [
            'full_name' => $this->full_name,
            'phone_no'=> $this->mobile_no,
            'email'=> $this->user->email,
            'street_address'=>$this->street_address,
            'location_id' =>$this->location,
            'profile_image_url'=>$this->profile_image_url,
            'user_information' =>$this->user_information

        ];
    }
}
