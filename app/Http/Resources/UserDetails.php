<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserDetails extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'phone_no'=> $this->mobile_no,
            'email'=> $this->user->email,
            'street_address'=>$this->street_address,
            'user_id'=>$this->user->id,
            'role'=>$this->user->role->role,
            'role_id'=>$this->user->role->id,
            'is_active'=>$this->user->is_active
        ];
    }
}
