<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Articles extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description'=> $this->description,
            'published_by'=> $this->user->email,
            'published_date'=> $this->published_data,
            'file_url'=>$this->file_url,
            'is_active'=>$this->is_active,
            'is_feature'=>$this->is_feature,
        ];
    }
}
