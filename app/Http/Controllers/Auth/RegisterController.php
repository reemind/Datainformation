<?php


namespace App\Http\Controllers\Auth;
use App\Notifications\UserRegisteredSuccessfully;
use App\User;
use App\UserDetails;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{

    use RegistersUsers;

    protected $redirectTo = '/home';

    
    public function __construct()
    {
        $this->middleware('guest');
    }

    
    protected function register(Request $request)
    {
        /** @var User $user */
        if($request->isMethod('put')){
            
            $validator = Validator::make($request->all(), [
                'email'    => 'required|string|email|max:255',
                'full_name' => 'required',
                'user_role' => 'required',
                'phone_no' => 'required'
               
            ]);
            if ($validator->fails()) {
                print('testss');
                return response()->json($validator->errors());
            }
            else{
              $userdetail = UserDetails::findOrFail($request->id);
              $userdetail->full_name = $request->input('full_name');
              $userdetail->mobile_no = $request->input('phone_no');
              $userdetail->save();
              $user = User::findOrFail($request->user_id);
              $user->email = $request->input('email');
              $user->role_id = $request->input('user_role');
              $user->is_active = $request->input('is_active');
              $user->save();
             return response()->json(['success' =>'User successfully Updated'],200);
            }

        } 
        else{
        $validatedData = $request->validate([
            'email'    => 'required|string|email|max:255|unique:users',
            'password' => 'required|string',
            'user_role' => 'required',
            'full_name' => 'required',
            'phone_no' => 'nullable|required',
            'is_active'=>'nullable'

        ]);
        try {
            $validatedData['password']        = bcrypt(array_get($validatedData, 'password'));
            $validatedData['verify_code'] = str_random(30).time();
            $validatedData['is_verified'] = false;
            $validatedData['role_id'] = $validatedData['user_role'];
            if($validatedData['is_active'] == '1')
                {
                    $validatedData['is_active'] = true;
                }
            else{
                $validatedData['is_active'] = false;
            }
            $user = app(User::class)->create($validatedData);
            
            $userdetail = new UserDetails;
            $userdetail->user_id = $user->id;
            $userdetail->full_name = $validatedData['full_name'];
            $userdetail->mobile_no = $validatedData['phone_no'];
            $userdetail->profile_image_url = 'images/default.jpg';
            $userdetail->save();
        } catch (\Exception $exception) {
            logger()->error($exception);
            return response()->json(['error' => $exception ],500);
        }
        $user->notify(new UserRegisteredSuccessfully($user));
        return response()->json(['success' =>'User successfully created'],200);
    }
}


    public function activateUser(string $activationCode)
    {
        try {
            $user = app(User::class)->where('verify_code', $activationCode)->first();
            if (!$user) {
                return "The code does not exist for any user in our system.";
            }
            $user->is_verified          = true;
            $user->verify_code = null;
            $user->save();
            auth()->login($user);
        } catch (\Exception $exception) {
            logger()->error($exception);
            return "Whoops! something went wrong.";
        }
        return redirect()->to('/');
    }

   
}