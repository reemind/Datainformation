<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\PasswordReset;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Notifications\AdminResetPasswordNotification;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    protected function sendPasswordResetLink(Request $request)
    {
        /** @var User $user */
        
        $validatedData = $request->validate([
            'email'    => 'required|string|email|max:255',
        ]);
        
        $passwordresetuser = User::where('email',$validatedData['email'])->get()[0];
        $passwordresetuser->remember_token = str_random(45).time();
        $passwordresetuser->save();
        $passwordresetuser->notify(new AdminResetPasswordNotification($passwordresetuser));
        return response()->json(['success' =>'User successfully find'],200);
    }


    public function showResetForm(string $token){
        try {
            $user = app(User::class)->where('remember_token', $token)->first();
            if (!$user) {
                return "The code does not exist for any user in our system.";
            }
            
        } catch (\Exception $exception) {
            logger()->error($exception);
            return "Whoops! something went wrong.";
        }
         return redirect()->to('passwordReset');
    }

}
