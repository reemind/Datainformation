<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use Validator;
use App\Http\Resources\News as NewsResource;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::where('is_active',true)
        ->orderBy('created_at', 'desc')->with('user')->paginate(3);
        if (!$news) {
            return  response()->json(['error' =>'No Item Was Found'],200);
        }
        return NewsResource::collection($news);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|unique:tb_news,title,'.$request->news_id,
            'description' => 'required',
            'published_date' => 'required',
        ]);
        if ($validator->fails()) {
            return  response()->json(['error' =>$validator->errors()],200);
        }
        else{
            $verifieduser= auth()->user();
            if($verifieduser){
                $news = $request->isMethod('put') ? News::findOrFail($request->news_id) : new News;
                $news->id = $request->input('news_id');
                $news->title = $request->input('title');
                $news->description = $request->input('description');
                $news->published_by = $verifieduser->id;
                $news->published_date = $request->input('published_date');

                $image = $request->input('file_url');
                if($image == NULL){
                    $news->file_url = 'images/default.jpg';
                }
                else
                {
                    if (strpos($image, 'images') !== false) {
                        $news->file_url = $image;
                    }
                    else{
                        $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                        \Image::make($request->input('file_url'))->save(public_path('images/').$name);
                    $news->file_url = 'images/'.$name;
                    }
                }
                if($request->input('is_active') == NULL){
                    $news->is_active = false;
                }
                else{
                $news->is_active = $request->input('is_active');
                }
                if($request->input('is_feature') == NULL){
                    $news->is_feature = false;
                }
                else{
                $news->is_feature = $request->input('is_feature');
                }
        
                if($news->save()){
                    return new NewsResource($news);
                }
            }
            
        }
       
    }

    public function NewsDelete($id){
        $del_news = News::findOrFail($id);
        if($del_news->delete()){
            return  response()->json(['success' =>'User has Deleted'],200);
        }
    }
}
