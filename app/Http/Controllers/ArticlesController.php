<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articles;
use Validator;
use App\Http\Resources\Articles as ArticlesResource;

class ArticlesController extends Controller
{
    public function index()
    {
        $articles = Articles::orderBy('created_at', 'desc')->where('is_active', true)->with('user')->paginate(2);
        if (!$articles) {
            return "No Items Was Found";
        }
        return ArticlesResource::collection($articles);
    }

    public function SearchItem($searchtxt){
        $articles = Articles::orderBy('created_at', 'desc')
        ->where('title','LIKE','%'.$searchtxt.'%')
        ->where('is_active', true)
        ->with('user')->paginate(2);
        if (!$articles) {
            return "No Items Was Found";
        }
        return ArticlesResource::collection($articles);
    }

    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|unique:tb_articles,title,'.$request->article_id,
            'description' => 'required',
            'published_date' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        else{
            $verifieduser= auth()->user();
            if($verifieduser){
                $article = $request->isMethod('put') ? Articles::findOrFail($request->article_id) : new Articles;
                $article->id = $request->input('article_id');
                $article->title = $request->input('title');
                $article->description = $request->input('description');
                $article->published_by =$verifieduser->id;
                $article->published_data = $request->input('published_date');
                $image = $request->input('file_url');
                if($image == NULL){
                    $article->file_url = 'images/default.jpg';
                }
                else
                {
                    if (strpos($image, 'images') !== false) {
                        $article->file_url = $image;
                    }
                    else{
                        $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                        \Image::make($request->input('file_url'))->save(public_path('images/').$name);
                    $article->file_url = 'images/'.$name;
                    }
                }
                if($request->input('is_active') == NULL){
                    $article->is_active = false;
                }
                else{
                $article->is_active = $request->input('is_active');
                }

                if($request->input('is_feature') == NULL){
                    $article->is_feature = false;
                }
                else{
                $article->is_feature = $request->input('is_feature');
                }
                if($article->save()){
                    return new ArticlesResource($article);
                }
            }
            else{
                return  response()->json(['error' =>'User is not Autheticate'],200);

            }
           
        }
       
    }
//only auth user can delete
    public function ArticleDelete($id){
        $del_article = Articles::findOrFail($id);
        if($del_article){
            $del_article->is_active = 0;
            $del_article->save();
            return  response()->json(['success' =>'Articles has Deleted'],200);
        }
    }
}