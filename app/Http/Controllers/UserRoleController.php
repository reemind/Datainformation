<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userrole;
use Validator;
use App\Http\Resources\Userrole as UserRoleResource;
class UserRoleController extends Controller
{
   
    public function index()
    {
        $role = Userrole::orderBy('created_at', 'desc')->paginate(50);
        return UserRoleResource::collection($role);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'role' => 'required|string|max:255',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        else{
            $user_role = $request->isMethod('put') ? Userrole::findOrFail($request->role_id) : new Userrole;
            $user_role->id = $request->input('role_id');
            $user_role->role = $request->input('role');
            if($request->input('is_active') == NULL){
                $user_role->is_active = false;
            }
            else{
            $user_role->is_active = $request->input('is_active');
            }
    
            if($user_role->save()){
                return new UserRoleResource($user_role);
            }
        }
       
    }
    
    public function show($id)
    {
        //
    }

    public function destroy($id)
    {
        $user_role = Userrole::findOrFail($id);
        if($user_role->delete()){
            return new UserRoleResource($user_role);
        }
    }
}
