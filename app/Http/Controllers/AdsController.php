<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ads;
use Validator;
use App\Http\Resources\Ads as AdsResource;
class AdsController extends Controller
{
    
    public function index()
    {
        $Adslist = Ads::where('is_active',true)
        ->where('published_from', '<=', date('Y-m-d'))
        ->where('published_to', '>=', date('Y-m-d'))
        ->orderBy('created_at', 'desc')->with('user')->paginate(10);
        if ($Adslist == NULL) {
            return  response()->json(['error' =>'No Item Was Found'],200);
        }
        return AdsResource::collection($Adslist);
    }

    public function SearchItem($searchtxt){
        $Adslist = Ads::where('is_active',true)
        ->where('ads_name','LIKE','%'.$searchtxt.'%')
        ->where('published_from', '<=', date('Y-m-d'))
        ->where('published_to', '>=', date('Y-m-d'))
        ->orderBy('created_at', 'desc')->with('user')->paginate(10);
        if ($Adslist == NULL) {
            return  response()->json(['error' =>'No Item Was Found'],200);
        }
        return AdsResource::collection($Adslist);
    }

    
    public function AdsStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ads_name' => 'required|string|unique:tb_ads,ads_name,'.$request->ads_id,
            'description' => 'required',
            'published_from'=>'required',
            'published_to'=>'required',
        ]);
        if ($validator->fails()) {
            return  response()->json(['error' =>'Validation Issue ']);
        }
        else{
            $verifieduser= auth()->user();
            if($verifieduser){
                $adslist = $request->isMethod('put') ? Ads::findOrFail($request->ads_id) : new Ads;
                $adslist->id = $request->input('ads_id');
                $adslist->ads_name = $request->input('ads_name');
                $adslist->description = $request->input('description');
                $adslist->published_by =$verifieduser->id;
                $adslist->published_from = $request->input('published_from');
                $adslist->published_to = $request->input('published_to');
                $image = $request->input('ads_image');
                if($image == NULL){
                    $adslist->ads_image = 'images/default.jpg';
                }
                else
                {
                    if (strpos($image, 'images') !== false) {
                        $adslist->ads_image = $image;
                    }
                    else{
                        $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                        \Image::make($request->input('ads_image'))->save(public_path('images/').$name);
                    $adslist->ads_image = 'images/'.$name;
                    }
                }
                if($request->input('is_active') == NULL){
                    $adslist->is_active = false;
                }
                else{
                $adslist->is_active = $request->input('is_active');
                }

                if($request->input('is_feature') == NULL){
                    $adslist->is_feature = false;
                }
                else{
                $adslist->is_feature = $request->input('is_feature');
                }
                if($adslist->save()){
                    return new AdsResource($adslist);
                }
            }
            else{
                return  response()->json(['error' =>'User is not Autheticate'],200);

            }
           
        }
       
    }

   
    
    public function AdsDelete($id){
        $adsdelete = Ads::findOrFail($id);
        if($adsdelete){
            $adsdelete->is_active = 0;
            $adsdelete->save();
            return  response()->json(['success' =>'Ads has Deleted'],200);
        }
    }
}
