<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Image;
use App\UserDetails;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Profiledata as ProfileResource;
use App\Http\Resources\UserDetails as UserDetailResource;


class UserController extends Controller
{
    
    public function index()
    {
        //get grade
        $userdetails = UserDetails::orderBy('created_at', 'desc')->with('user','user.role')->paginate(5);
        return UserDetailResource::collection($userdetails);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'full_name' => 'required',
            'password'=> 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        User::create([
            'name' => $request->get('full_name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);
        $user = User::first();
        $token = JWTAuth::fromUser($user);
        
        return Response::json(compact('token'));
    }

    public function GetProfileData(Request $request){

        $verifieduser= auth()->user();
        if($verifieduser){
            $userdetail = UserDetails::where('user_id', $verifieduser->id)->with('user','location')->first();
            return new ProfileResource($userdetail);
        }
        else
        {
            return response()->json(['error' =>'User has no data'],200);
        }

    }

    public function UpdateProfileData(Request $request){
        $validator = Validator::make($request->all(), [
            // 'email' => 'required|string|email|max:255|unique:users',
            'phone_no' => 'required',
            'street_address' => 'required',
            'profile_image_url' => 'nullable',
            'user_information' => 'nullable',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        else
        {
            
            $verifieduser= auth()->user();
            $userdetail = UserDetails::where('user_id', $verifieduser->id)->with('user','location')->first();
            $userdetail->mobile_no = $request->get('phone_no');
            $userdetail->full_name = $request->get('full_name');
            $userdetail->street_address = $request->get('street_address');
            $userdetail->user_information = $request->get('user_information');

            $image = $request->input('profile_image_url');
            if($image == NULL){
                $userdetail->profile_image_url = 'images/default.jpg';
            }
            else
            {
                if (strpos($image, 'images') !== false) {
                    $userdetail->profile_image_url = $image;
                }
                else{
                    $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                    \Image::make($request->input('profile_image_url'))->save(public_path('images/').$name);
                $userdetail->profile_image_url = 'images/'.$name;
                }
            }

            $userdetail->save();
            return response()->json(['success' =>'User has Updated'],200);
            // $userdetail->user->email = $request->get('email');
        }
    }

    public function UserDelete($id){
        $del_user = User::findOrFail($id);
        if($del_user->delete()){
            return  response()->json(['success' =>'User has Deleted'],200);
        }
    }
    
}
