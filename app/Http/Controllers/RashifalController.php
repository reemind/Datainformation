<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rashifal;
use Validator;
use App\Http\Resources\Rashifal as RashifalResource;
class RashifalController extends Controller
{
    public function index()
    {
        //->where('published_from', '<=', date('Y-m-d'))
        //->where('published_to', '>=', date('Y-m-d'))
        $rashifal = Rashifal::where('is_active',true)
            ->orderBy('created_at', 'desc')->with('user')->paginate(3);
        if (!$rashifal) {
            return  response()->json(['error' =>'No Item Was Found'],200);
        }
        return RashifalResource::collection($rashifal);
    }

    public function RashifalPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'published_from' => 'required',
            'published_to' => 'required'
        ]);
        if ($validator->fails()) {
            return  response()->json(['error' =>$validator->errors()],200);
        }
        else{
            $verifieduser= auth()->user();
            if($verifieduser){
                $rashifal = $request->isMethod('put') ? Rashifal::findOrFail($request->rashifal_id) : new Rashifal;
                $rashifal->id = $request->input('rashifal_id');
                $rashifal->description = $request->input('description');
                $rashifal->published_by = $verifieduser->id;
                $rashifal->type = $request->input('type');
                $rashifal->published_from = $request->input('published_from');
                $rashifal->published_to = $request->input('published_to');
                $rashifal->aries = $request->input('aries');
                $rashifal->taurus = $request->input('taurus');
                $rashifal->gemini = $request->input('gemini');
                $rashifal->cancer = $request->input('cancer');
                $rashifal->leo = $request->input('leo');
                $rashifal->virgo = $request->input('virgo');
                $rashifal->libra = $request->input('libra');
                $rashifal->scorpio = $request->input('scorpio');
                $rashifal->sagittarius = $request->input('sagittarius');
                $rashifal->capricorn = $request->input('capricorn');
                $rashifal->aquarius = $request->input('aquarius');
                $rashifal->pisces = $request->input('pisces');

                if($request->input('is_active') == NULL){
                    $rashifal->is_active = false;
                }
                else{
                $rashifal->is_active = $request->input('is_active');
                }

                if($rashifal->save()){
                    return new RashifalResource($rashifal);
                }
            }
            }
    }

    

    public function RashifalDelete($id){
        $adsdelete = Rashifal::findOrFail($id);
        if($adsdelete){
            $adsdelete->is_active = 0;
            $adsdelete->save();
            return  response()->json(['success' =>'Rashifal has Deleted'],200);
        }
    }
}