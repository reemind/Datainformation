<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrganizationInfo;
use App\State;
use App\District;
use App\Address;
use App\Location;//location is represent as municipality
use Validator;
use App\Http\Resources\OrganizationInfo as OrganizationInfoResource;
use App\Http\Resources\OrganizationBasicInfo as OrganizationBasicInfoResource;
use App\Http\Resources\State as StateResource;
use App\Http\Resources\District as DistrictResource;
use App\Http\Resources\Municipality as MunicipalityResource;

class OrganizationInfoConlroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizationInfo = OrganizationInfo::where('is_verified',true)
            ->where('is_active', true)
            ->orderBy('created_at', 'desc')->with('category','user','address','address.municipality',
            'address.municipality.district','address.municipality.district.state')->paginate(300);
        if (!$organizationInfo) {
            return  response()->json(['error' =>'No Item Was Found At this moment'],200);
        }   
        return OrganizationInfoResource::collection($organizationInfo);
    }
    public function OrganizationBasicInfo()
    {
        $organizationInfo = OrganizationInfo::where('is_verified',true)
            ->where('is_active', true)
            ->orderBy('created_at', 'desc')->with('category','user','address','address.municipality',
            'address.municipality.district','address.municipality.district.state')->paginate(500);
        if (!$organizationInfo) {
            return  response()->json(['error' =>'No Item Was Found At this moment'],200);
        }   
        return OrganizationBasicInfoResource::collection($organizationInfo);
    }

    
    public function SearchItem($searchtxt){
        $organizationInfo = OrganizationInfo::where('is_verified',true)
        ->where('is_active', true)
        ->where('organization_name','LIKE','%'.$searchtxt.'%')
        ->orderBy('created_at', 'desc')->with('category','user','address','address.municipality',
        'address.municipality.district','address.municipality.district.state')->paginate(3);
        if ($organizationInfo == NULL) {
            return  response()->json(['error' =>'No Item Was Found'],200);
        }
        return OrganizationInfoResource::collection($organizationInfo);
    }

    public function StateAddress(){
        $state = State::where('is_active',true)->paginate(50);

        if(!$state){
            return  response()->json(['error' =>'No Item Was Found At this moment'],200);
        }
        return StateResource::collection($state);

    }

    public function GetDistrictByStateId($stateid){
        $district = District::where('state_id',$stateid)->get();

        if(!$district){
            return  response()->json(['error' =>'No Item Was Found At this moment'],200);
        }
        return DistrictResource::collection($district);

    }

    public function GetMunicipalityByDistrictId($districtid){
        $municipality = Location::where('district_id',$districtid)->get();

        if(!$municipality){
            return  response()->json(['error' =>'No Item Was Found At this moment'],200);
        }
        return MunicipalityResource::collection($municipality);
    }

    public function StoreNewOrganizationInfo(Request $request){
        $validator = Validator::make($request->all(), [
            'category_id'=>'required',
            'organization_name' => 'required|string|unique:tb_organizationinfo,organization_name,'.$request->organization_id,
            'phone_no'=>'required',
            'organization_email'=>'required',
            'addstate_id'=>'required',
            'district_id'=>'required',
            'municipality_id'=>'required',
            'street_address'=>'required',
            'services'=>'required',
            'about_organization'=>'required',
            // 'profile_image'=>'required',
            // 'banner_image'=>'required',
        ]);
        if ($validator->fails()) {
            return  response()->json(['error' =>$validator->errors()],200);
        }
        $verifieduser= auth()->user();
        if($verifieduser){
           

                $organizationinfo = $request->isMethod('put') ? OrganizationInfo::findOrFail($request->organization_id) : new OrganizationInfo;
                $organizationinfo->id = $request->input('organization_id');
                $organizationinfo->category_id = $request->input('category_id');
                $organizationinfo->organization_name = $request->input('organization_name');
                $organizationinfo->phone_no = $request->input('phone_no');
                $organizationinfo->organization_email = $request->input('organization_email');
                $organizationinfo->services = $request->input('services');
                $organizationinfo->about_organization = $request->input('about_organization');
                $organizationinfo->feature = $request->input('feature');
                $organizationinfo->website = $request->input('website');
                $organizationinfo->related_links = $request->input('related_links');
                $organizationinfo->openning_day = $request->input('openning_day');
                $organizationinfo->openning_time = $request->input('openning_time');
                $organizationinfo->added_by =$verifieduser->id;
                
                $profileimage = $request->input('profile_image');
                if($profileimage == NULL){
                    $organizationinfo->profile_image = 'images/default.jpg';
                }
                else
                {
                    if (strpos($profileimage, 'images') !== false) {
                        $organizationinfo->profile_image = $profileimage;
                    }
                    else{
                        $name = time().'.' . explode('/', explode(':', substr($profileimage, 0, strpos($profileimage, ';')))[1])[1];
                        \Image::make($request->input('profile_image'))->save(public_path('images/').$name);
                    $organizationinfo->profile_image = 'images/'.$name;
                    }
                }

                $bannerimage = $request->input('banner_image');
                if($bannerimage == NULL){
                    $organizationinfo->banner_image = 'images/default.jpg';
                }
                else
                {
                    if (strpos($bannerimage, 'images') !== false) {
                        $organizationinfo->banner_image = $bannerimage;
                    }
                    else{
                        $name = time().'.' . explode('/', explode(':', substr($bannerimage, 0, strpos($bannerimage, ';')))[1])[1];
                        \Image::make($request->input('banner_image'))->save(public_path('images/').$name);
                    $organizationinfo->banner_image = 'images/'.$name;
                    }
                }


                if($request->input('is_verified') == NULL){
                    $organizationinfo->is_verified = false;
                }
                else{
                $organizationinfo->is_verified = $request->input('is_verified');
                }
                if($request->input('is_feature') == NULL){
                    $organizationinfo->is_feature = false;
                }
                else{
                $organizationinfo->is_feature = $request->input('is_feature');
                }

                if($request->input('is_active') == NULL){
                    $organizationinfo->is_active = false;
                }
                else{
                $organizationinfo->is_active = $request->input('is_active');
                }

                if($request->isMethod('put')){
                    
                    $addressdetails = Address::where('id', $request->input('street_address_id'))->first();
                    
                    $addressdetails->street_address = $request->input('street_address');
                    $addressdetails->lattitude = $request->input('lattitude');
                    $addressdetails->longitude = $request->input('longitude');
                    $addressdetails->municipality_id = $request->input('municipality_id');
                    
                    $addressdetails->save();
                 
                    $organizationinfo->address_id = $addressdetails->id;
                }
                else{
                   
                    $addressdetails = new Address();
                $addressdetails->street_address = $request->input('street_address');
                $addressdetails->lattitude = $request->input('lattitude');
                $addressdetails->longitude = $request->input('longitude');
                $addressdetails->municipality_id = $request->input('municipality_id');
                $addressdetails->save();
                $organizationinfo->address_id = $addressdetails->id;
                }
                
               
                

                 
                if($organizationinfo->save()){
                    return new OrganizationInfoResource($organizationinfo);
                }
        }
        else{
            return  response()->json(['error' => 'User Is not Authenticate']);
        }
    }

    public function DeleteOrganizationInfo($id){
        $organizationinfo = OrganizationInfo::findOrFail($id);
        $org_addredd = $organizationinfo->address_id;
        $address_delete = Address::findOrFail($org_addredd);
        $address_delete->delete();
        if($organizationinfo->delete()){
            return  response()->json(['success' =>'User has Deleted'],200);
        }
    }

    public function OrganizationInfoByCategory($catid, $location,$query){
        $organizationInfo = OrganizationInfo::where('is_verified',true)
            ->where('is_active', true)
            ->where('category_id',$catid)
            ->where('organization_name','Like',"%$query%")
            ->orderBy('created_at', 'desc')->with('category','user','address','address.municipality',
            'address.municipality.district','address.municipality.district.state')->paginate(3);
        if (!$organizationInfo) {
            return  response()->json(['error' =>'No Item Was Found At this moment'],200);
        }   
        return OrganizationInfoResource::collection($organizationInfo);

    }
   

    
}