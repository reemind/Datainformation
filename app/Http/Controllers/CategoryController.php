<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Validator;
use App\Http\Resources\Category as CategoryResource;
class CategoryController extends Controller
{
   
    public function index()
    {
        $category = Category::orderBy('created_at', 'desc')->paginate(50);
        if (!$category) {
            return "No Items Was Found";
        }
        return CategoryResource::collection($category);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_name' => 'required|string|unique:tb_category,category_name,'.$request->category_id,
        ]);
        if ($validator->fails()) {
            return  response()->json(['error' =>$validator->errors()],200);
        }
        else{
            $category = $request->isMethod('put') ? Category::findOrFail($request->category_id) : new Category;
            $category->id = $request->input('category_id');
            $category->category_name = $request->input('category_name');
            $image = $request->input('category_image');
                if($image == NULL){
                    $category->category_image = 'images/default.jpg';
                }
                else
                {
                    if (strpos($image, 'images') !== false) {
                        $category->category_image = $image;
                    }
                    else{
                        $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                        \Image::make($request->input('category_image'))->save(public_path('images/').$name);
                    $category->category_image = 'images/'.$name;
                    }
                }
            if($request->input('is_active') == NULL){
                $category->is_active = false;
            }
            else{
            $category->is_active = $request->input('is_active');
            }
    
            if($category->save()){
                return new CategoryResource($category);
            }
        }
       
    }
    
    // public function show($id)
    // {
    //     //
    // }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        if($category->delete()){
            return new CategoryResource($category);
        }
    }
}
