<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VideoLink;
use Validator;
use App\Http\Resources\VideoLink as VideoLinkResource;


class VideolinkController extends Controller
{
    public function index()
    {
        $videolinks = VideoLink::orderBy('created_at', 'desc')->paginate(10);
        if(!$videolinks){
            return  response()->json(['error' =>'No Item Was Found'],200);
        }
        return VideoLinkResource::collection($videolinks);
    }

   
    public function VideoLinkStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'video_title' => 'required|string|unique:tb_videolink,video_title,'.$request->videolink_id,
            'videolink' => 'required'
        ]);
        if ($validator->fails()) {
            return  response()->json(['error' =>$validator->errors()],200);
        }
        else{
                $videolink = $request->isMethod('put') ? VideoLink::findOrFail($request->videolink_id) : new VideoLink;
                $videolink->id = $request->input('videolink_id');
                $videolink->videolink = $request->input('videolink');
                $videolink->video_title = $request->input('video_title');
                $videolink->description = $request->input('description');
                if($request->input('is_active') == NULL){
                    $videolink->is_active = false;
                }
                else{
                $videolink->is_active = $request->input('is_active');
                }
                if($videolink->save()){
                    return new VideoLinkResource($videolink);
                }
        }
    }

    
    public function VideoLinkDelete($id)
    {
        $videolink = VideoLink::findOrFail($id);
        if($videolink->delete()){
            return new VideoLinkResource($videolink);
        }
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
