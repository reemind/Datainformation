<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Notifications\AdminResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id','email', 'username', 'password' , 'is_verified', 'verify_code','remember_token','is_active'
    ];

    public function role(){
    	return $this->belongsTo('App\Userrole','role_id');
    } 
    
}
