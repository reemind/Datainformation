<?php

use Faker\Generator as Faker;

$factory->define(App\Question::class, function ($faker) use ($factory){
    return [
        'grade_id' => $factory->create(App\Grade::class)->id,
        'subject_id' => $factory->create(App\Subject::class)->id,
        'chapter_id' => $factory->create(App\Chapter::class)->id,
        'question_type_id' => $factory->create(App\Question_type::class)->id,
        'location_id' => $factory->create(App\Location::class)->id,
        'question' => $faker->text(100),
        'answer_short' => $faker->text(20),
        'answer_long' => $faker->text(200),
        'hints' => $faker->text(50),
        'marks' => $faker->randomDigit
    ];
});
