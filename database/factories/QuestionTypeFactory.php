<?php

use Faker\Generator as Faker;

$factory->define(App\Question_type::class, function (Faker $faker) {
    return [
        'question_type_name' => $faker->text(20)
    ];
});
