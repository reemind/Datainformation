<?php

use Faker\Generator as Faker;

$factory->define(App\Subject::class, function ($faker) use ($factory){
    return [
        'grade_id' => $factory->create(App\Grade::class)->id,
        'subject_name' => $faker->text(20)
    ];
});
