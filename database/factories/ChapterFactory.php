<?php

use Faker\Generator as Faker;

$factory->define(App\Chapter::class, function ($faker) use ($factory){
    return [
        'grade_id' => $factory->create(App\Grade::class)->id,
        'subject_id' => $factory->create(App\Subject::class)->id,
        'chapter_name' => $faker->text(20),
        'chapter_description' => $faker->text(50),
        'study_material' => $faker->text(100)
    ];
});
