<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabbleVDCMunicipality extends Migration
{
    
    public function up()
    {
        Schema::create('tb_vdc_municipality', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('state_id');
            $table->foreign('state_id')->references('id')->on('tb_state')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedInteger('district_id');
            $table->foreign('district_id')->references('id')->on('tb_district')->onDelete('cascade')->onUpdate('cascade');
            $table->string('vdv_municipality_name');
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tb_vdc_municipality');
    }
}
