<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCategory extends Migration
{
   
    public function up()
    {
        Schema::create('tb_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category_name');
            $table->boolean('is_active')->default(false);
            $table->timestamps();
        });
    }

  
    public function down()
    {
        Schema::dropIfExists('tb_category');
    }
}
