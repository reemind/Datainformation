<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabbleUserDetails extends Migration
{
    public function up()
    {
        Schema::create('tb_user_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('full_name')->nullable();
            $table->string('street_address')->nullable();
            $table->unsignedInteger('location_id')->nullable();
            $table->foreign('location_id')->references('id')->on('tb_vdc_municipality')->onDelete('cascade')->onUpdate('cascade');
            $table->string('profile_image_url')->nullable();
            $table->string('user_information')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tb_user_details');
    }
}
