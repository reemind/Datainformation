<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrganizationAddressDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_organization_address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('street_address');
            $table->string('lattitude')->nullable();
            $table->string('longitide')->nullable();
            $table->unsignedInteger('vnc_municipality_id');
            $table->foreign('vnc_municipality_id')->references('id')->on('tb_vdc_municipality')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_organization_address');
    }
}
