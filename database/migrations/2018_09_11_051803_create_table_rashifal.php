<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRashifal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_rashifal', function (Blueprint $table) {
            $table->increments('rashifal_id');
            $table->unsignedInteger('published_by');
            $table->foreign('published_by')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->enum('type', ['Day', 'Week','Month','Year']);
            $table->date('published_from')->nullable();
            $table->date('published_to')->nullable();
            $table->longText('description')->nullable();
            $table->string('aries')->nullable();
            $table->string('taurus')->nullable();
            $table->string('gemini')->nullable();
            $table->string('cancer')->nullable();
            $table->string('leo')->nullable();
            $table->string('virgo')->nullable();
            $table->string('libra')->nullable();
            $table->string('scorpio')->nullable();
            $table->string('sagittarius')->nullable();
            $table->string('capricorn')->nullable();
            $table->string('aquarius')->nullable();
            $table->string('pisces')->nullable();
            $table->boolean('is_active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rashifal');
    }
}
