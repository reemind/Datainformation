<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAds extends Migration
{
   
    public function up()
    {
        Schema::create('tb_ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ads_name');
            $table->string('description')->nullable();
            $table->unsignedInteger('published_by');
            $table->foreign('published_by')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('ads_image');
            $table->date('published_from')->nullable();
            $table->date('published_to')->nullable();
            $table->boolean('is_active')->default(false);
            $table->boolean('is_feature')->default(false);
            $table->timestamps();
        });
    }

   
    public function down()
    {
         Schema::dropIfExists('tb_ads');
    }
}
