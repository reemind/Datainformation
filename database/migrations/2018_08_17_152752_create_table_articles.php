<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableArticles extends Migration
{
   
    public function up()
    {
        Schema::create('tb_articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('description')->nullable();
            $table->unsignedInteger('published_by');
            $table->foreign('published_by')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('file_url')->nullable();
            $table->date('published_data')->nullable();
            $table->boolean('is_active')->default(false);
            $table->boolean('is_feature')->default(false);
            $table->timestamps();
        });
    }

   
    public function down()
    {
         Schema::dropIfExists('tb_articles');
    }
}
