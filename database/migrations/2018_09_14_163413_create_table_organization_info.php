<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrganizationInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_organizationinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('tb_category')->onDelete('cascade')->onUpdate('cascade');
            $table->string('organization_name');
            $table->string('phone_no');
            $table->string('organization_email')->nullable();
            $table->unsignedInteger('address_id');
            $table->foreign('address_id')->references('id')->on('tb_organization_address')->onDelete('cascade')->onUpdate('cascade');
            $table->string('openning_time')->nullable();
            $table->string('openning_day')->nullable();
            $table->longText('services')->nullable();
            $table->longText('feature')->nullable();
            $table->longText('about_organization');
            $table->string('website')->nullable();
            $table->string('related_links')->nullable();
            $table->string('banner_image')->nullable();
            $table->string('profile_image');
            $table->unsignedInteger('added_by');
            $table->foreign('added_by')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('is_verified')->default(false);
            $table->boolean('is_active')->default(false);
            $table->boolean('is_feature')->default(false);
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_organizationinfo');
    }
}
