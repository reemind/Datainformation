<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  $this->call(GradeTableSeeder::class);
        //  $this->call(SubjectTableSeeder::class);
        // $this->call(ChapterTableSeeder::class);
        $this->call(QuestionTableSeeder::class);
    }
}
