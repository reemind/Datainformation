<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('{path}', function () {
//     return view('landing');
// })

Route::get('/', function () { return view('landing');});
Route::get('/front', function () { return view('landing');});
Route::get('/map', function () { return view('landing');});
Route::get('/orginfobycategory{path}', function () { return view('landing');})->where( 'path', '([A-z\d-\/_.]+)?' );
Route::get('/searchlistingpage', function () { return view('landing');});
Route::get('/organizationdetails/{path}', function () { return view('landing');})->where( 'path', '([A-z\d-\/_.]+)?' );
Route::get('/login', function () { return view('landing');});
Route::get('/admin', function () {return view('landing');});
Route::get('/organizationinfolisting', function () {return view('landing');});
Route::get('/addedit_organizationinfo', function () {return view('landing');});
Route::get('/videolinkmanage', function () {return view('landing');});
Route::get('/adslisting', function () {return view('landing');});
Route::get('/addedit_ads', function () {return view('landing');});
Route::get('/articleslisting', function () {return view('landing');});
Route::get('/addedit_articles', function () {return view('landing');});
Route::get('/rashifallisting', function () {return view('landing');});
Route::get('/addedit_rashifal', function () {return view('landing');});
Route::get('/usermanage', function () {return view('landing');});
Route::get('/category', function () {return view('landing');});
Route::get('/RoleManage', function () {return view('landing');});
Route::get('/newslisting', function () {return view('landing');});
Route::get('/addedit_news', function () {return view('landing');});




Route::get('/home', 'HomeController@index')->name('home');


