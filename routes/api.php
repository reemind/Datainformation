<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::middleware('jwt.auth')->get('users', function(Request $request) {
//     return auth()->user();
// });
Route::middleware('jwt.auth')->get('chapters', function(Request $request) {
    return auth()->user();
});
Route::middleware('jwt.auth')->group(function () {
    
    Route::get('dashboard', function () {
        return response()->json(['data' => 'Test Data']);
    });

    Route::post('user','UserController@register');
    Route::delete('user/{id}','UserController@UserDelete');
    // Route::get('users','UserController@index');

    
    Route::post('/password/reset', 'Auth\ResetPasswordController@PasswordReset');
    Route::get('/profile_data', 'UserController@GetProfileData');
    Route::post('/profile_data', 'UserController@UpdateProfileData');

    Route::get('newslist','NewsController@index');
    Route::put('news','NewsController@store');
    Route::post('news','NewsController@store');
    Route::delete('news/{id}','NewsController@NewsDelete');

    // Route::get('articles','ArticlesController@index');
    Route::get('articlesearchcontain/{searchtxt}','ArticlesController@SearchItem');
    Route::put('article','ArticlesController@store');
    Route::post('article','ArticlesController@store');
    Route::delete('article/{id}','ArticlesController@ArticleDelete');
    
    //Ads Manage api
    Route::get('adslist','AdsController@index');
    Route::get('searchcontain/{searchtxt}','AdsController@SearchItem');
    Route::delete('ads/{id}','AdsController@AdsDelete');
    Route::post('ads','AdsController@AdsStore');
    Route::put('ads','AdsController@AdsStore');

    // Rashifal Manage
    Route::delete('rashifal/{id}','RashifalController@RashifalDelete');
    Route::POST('rashifal','RashifalController@RashifalPost');
    Route::put('rashifal','RashifalController@RashifalPost');
    // Route::get('user_roles','UserRoleController@index');
Route::put('register','Auth\RegisterController@register');
Route::post('register','Auth\RegisterController@register');

Route::get('organizationinfosearchcontain/{searchtxt}','OrganizationInfoConlroller@SearchItem');
// Route::get('orgonizationlist','OrganizationInfoConlroller@index');
Route::get('address_state','OrganizationInfoConlroller@StateAddress');
Route::get('getdistrictbystateid/{id}','OrganizationInfoConlroller@GetDistrictByStateId');
Route::get('getmunicipalitybydistrictid/{id}','OrganizationInfoConlroller@GetMunicipalityByDistrictId');
Route::post('neworganizationinfo','OrganizationInfoConlroller@StoreNewOrganizationInfo');
Route::put('editorganizationinfo','OrganizationInfoConlroller@StoreNewOrganizationInfo');
Route::delete('organizationinfo/{id}','OrganizationInfoConlroller@DeleteOrganizationInfo');

});

Route::get('orgonizationlist','OrganizationInfoConlroller@index');

//Video Links
Route::get('videolinks','VideolinkController@index');
Route::post('videolink','VideolinkController@VideoLinkStore');
Route::put('videolink','VideolinkController@VideoLinkStore');
Route::delete('videolink/{id}','VideolinkController@VideoLinkDelete');

Route::get('rashifals','RashifalController@index');


Route::get('users','UserController@index');

Route::get('user_roles','UserRoleController@index');
Route::put('user_role','UserRoleController@store');
Route::post('user_role','UserRoleController@store');
Route::delete('user_role/{id}','UserRoleController@destroy');


Route::get('categories','CategoryController@index');
Route::put('category','CategoryController@store');
Route::post('category','CategoryController@store');
Route::delete('category/{id}','CategoryController@destroy');

Route::get('organizationbasicinfo','OrganizationInfoConlroller@OrganizationBasicInfo');

 Route::get('organizationinfobycategory/Category_id={catID}/Location={location}/Query_text={query}','OrganizationInfoConlroller@OrganizationInfoByCategory');
//  Route::get('address_state','OrganizationInfoConlroller@StateAddress');
//  Route::get('getdistrictbystateid/{id}','OrganizationInfoConlroller@GetDistrictByStateId');
//  Route::get('getmunicipalitybydistrictid/{id}','OrganizationInfoConlroller@GetMunicipalityByDistrictId');
//  Route::post('neworganizationinfo','OrganizationInfoConlroller@StoreNewOrganizationInfo');
// Route::delete('ads/{id}','AdsController@AdsDelete');
// Route::post('ads','AdsController@AdsStore');

// Route::put('news','NewsController@store');
// Route::post('news','NewsController@store');
// Route::delete('news/{id}','NewsController@NewsDelete');

Route::get('articles','ArticlesController@index');
// Route::put('article','ArticlesController@store');
// Route::post('article','ArticlesController@store');
// Route::delete('article/{id}','ArticlesController@ArticleDelete');

Route::get('locations','LocationController@index');






// Auth::routes();
//user verify
Route::get('/verify-user/{code}', 'Auth\RegisterController@activateUser')->name('activate.user');
//user register
Route::post('register', 'Auth\RegisterController@register');
Route::put('register', 'Auth\RegisterController@register');
//user login
Route::post('user/login', 'APILoginController@login');

//Routes list
 // Password reset routes
 Route::post('/password/email', 'Auth\ForgotPasswordController@sendPasswordResetLink');
 
 Route::get('/password/reset/{token}', 'Auth\ForgotPasswordController@showResetForm')->name('password.reset');
 
 Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
