/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
/*jshint esversion: 6 */
require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import VueRouter from 'vue-router';

import VueCkeditor from 'vue-ckeditor2';

import Login from './components/Login';
import LandingPage from './components/LandingPage';
import Logout from './components/Logout';
import UserRegister from './components/Usermanage/UserRegister';
import UserManage from './components/Usermanage/UserManage';
import AddUserRegister from './components/Usermanage/AddUserRegister';
import ManageRole from './components/RoleManage/ManageRole';
import ForgotPassword from './components/ForgotPassword/ForgotPassword';
import ResetPassword from './components/PasswordReset/PasswordReset';
import ProfileUpdate from './components/ProfileManage/ProfileUpdate';
import ComponentView from './components/ComponentView';

import Category from './components/Category/Category';
import NewsManage from './components/NewsManage/NewsListing'
import NewsAddEditManage from './components/NewsManage/AddEditNews';
import ArticlesManage from './components/ArticlesManage/ArticlesListing';
import ArticlesAddEditManage from './components/ArticlesManage/AddEditArticles';

import AdsManage from './components/AdsManage/AdsListing';
import AdsAddEditManage from './components/AdsManage/AddEditAds';

import RashifalManage from './components/RashifalManage/RashifalListing';
import RashifalAddEditManage from './components/RashifalManage/AddEditRashifal'

import OrganizationInfoManage from './components/OrganizationInfoManage/OrganizationInfoList'
import NewsAddEditOrganizationInfo from './components/OrganizationInfoManage/OrganizationInfoAddEdit'
import validationtest from './components/Validatepage';
import TestListPages from './components/ValidateListPage';
import VideoLinkManage from './components/VideoLinkManage/VideoLinkManage';

import FrontLandingPage from './components/FrontEnd/FLandingPage';
import HomePage from './components/FrontEnd/HomePage';
import OrganizationDetailsPage from './components/FrontEnd/OrganizationDetailsPage';
import SearchListingPage from './components/FrontEnd/SearchListingPage';
import OrgInfoByCategory from './components/FrontEnd/OrgInfoByCategory';
import FrontComponentView from './components/FrontEnd/FrontComponentView';

import 'keen-ui/dist/keen-ui.css';
import KeenUI from 'keen-ui';
import Element from "element-ui";
import en from "element-ui/lib/locale/lang/en";
Vue.use(Element, { locale: en });
import 'element-ui/lib/theme-chalk/index.css';
Vue.component('Componentview', require('./components/ComponentView'));
Vue.component('navbar', require('./components/Navbar'));
Vue.component('sidebar', require('./components/Sidebar'));
Vue.component('visualbar', require('./components/FrontEnd/Visualbar'));
Vue.component('articlecomponent', require('./components/FrontEnd/ArticleComponent'));
Vue.component('organizationhomepagecard', require('./components/FrontEnd/Organizationhomepage'));
Vue.component('footerhomepage', require('./components/FrontEnd/footer'));
Vue.component('feedback', require('./components/FrontEnd/Feedbackcomponent'));
Vue.component('category', require('./components/FrontEnd/Categorycomponent'));

Vue.use(VueRouter);
Vue.use(VueCkeditor);
Vue.use(KeenUI);
import * as VueGoogleMaps from "vue2-google-maps";
Vue.use(VueGoogleMaps, {
    load: {
      key: "AIzaSyB8ev_CY3aAR4nxwVX5fM8m51OytzvFAgc",
      libraries: "places" // necessary for places input
    }
  });
import{ Message,Loading} from 'element-ui';
Vue.use(Loading.directive);
Vue.prototype.$loading = Loading.service;
Vue.prototype.$message = Message;
import GoogleMap from './components/GoogleMap'
Vue.filter('striphtml', function (value) {
    var div = document.createElement("div");
    div.innerHTML = value;
    var text = div.textContent || div.innerText || "";
    return text;
  });

  
const routes = [
   
    {
        path: '/searchlistingpage',
        component: SearchListingPage,
        name: 'details'
    },
    {
        path: '/map',
        component: GoogleMap,
        name: 'details'
    },
    {
        path: '/organizationdetails/:slog',
        component: OrganizationDetailsPage,
        name: 'organizationdetails',
        props:true
    },
    // {
    //     path: '/front',
    //     component: FrontLandingPage,
    //     name: 'frontend'
    // },
    // {
    //     path: '/',
    //     component: HomePage,
    //     name: 'frontend'
    // },
    {
        path: '/login',
        component: Login,
        name: 'login'
    },
    {
        path: '/',
        component: FrontComponentView,
        children:[{
            path:'',
            name: 'frontend',
            component:HomePage,
            props: true,
        },
        {
            path: '/orginfobycategory/:category',
            component: OrgInfoByCategory,
            name: 'orginfobycategory',
            props: true,
        },]
       
    },

    {
        path: '/admin',
        component: ComponentView,
        children: [{
                path: '',
                name: 'LandingPage',
                component: LandingPage,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/profile',
                name: 'ProfileUpdate',
                component: ProfileUpdate,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/testlist',
                name: 'TestListPages',
                component: TestListPages,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/videolinkmanage',
                name: 'VideoLinkManage',
                component: VideoLinkManage,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/newslisting',
                name: 'NewsManage',
                component: NewsManage,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/validationtest',
                name: 'validationtest',
                props: true,
                component: validationtest,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/addedit_news',
                name: 'addedit_news',
                props: true,
                component: NewsAddEditManage,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/rashifallisting',
                name: 'RashifalManage',
                component: RashifalManage,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/organizationinfolisting',
                name: 'OrganizationInfoManage',
                component: OrganizationInfoManage,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/addedit_organizationinfo',
                name: 'addedit_organizationinfo',
                props: true,
                component: NewsAddEditOrganizationInfo,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/addedit_rashifal',
                name: 'addedit_rashifal',
                props: true,
                component: RashifalAddEditManage,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/articleslisting',
                name: 'ArticlesManage',
                component: ArticlesManage,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/addedit_articles',
                name: 'addedit_articles',
                props: true,
                component: ArticlesAddEditManage,
                meta: {
                    requiresAuth: true
                }
            },

            {
                path: '/adslisting',
                name: 'AdsManage',
                component: AdsManage,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/addedit_ads',
                name: 'addedit_ads',
                props: true,
                component: AdsAddEditManage,
                meta: {
                    requiresAuth: true
                }
            },
            
            {
                path: '/category',
                name: 'category',
                component: Category,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/usermanage',
                name: 'UserManage',
                component: UserManage,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/passwordReset',
                component: ResetPassword,
                name: 'resetpassword',
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/forgotpassword',
                component: ForgotPassword,
                name: 'forgotpassword'
            },
            {
                path: '/RoleManage',
                name: 'RoleManage',
                component: ManageRole,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/profileupdate',
                name: 'profileupdate',
                component: ProfileUpdate,
                meta: {
                    requiresAuth: true
                }
            },

            {
                path: '/logout',
                name: "logout",
                component: Logout,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/user_register',
                name: "user_register",
                component: UserRegister,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/addedit_user_register',
                name: "addedit_user_register",
                props: true,
                component: AddUserRegister,
                meta: {
                    requiresAuth: true
                }
            },
        ]
    },
    
];
const router = new VueRouter({
    routes,
    mode: 'history'
});
router.beforeEach((to, from, next) => {
    if (to.meta.requiresAuth) {
        if (localStorage.access_token) {
            next()
        } else {
            next({
                name: 'login'
            })
        }
    }
    next()
});

const app = new Vue({
    router
}).$mount('#app');